# Unit test for IoT shield version 2
This sketch is designed to interface with version 2 of the shield. 

With **version 2** of the shield, hardware connections to the The Things Uno board have been changed to enable:
 - Softserial use on the Groove connectors, adding a extra (3rd) serial port.
 - Use of interrupts with the buttons.
 
# Notes
 - The unit test will test hardware on board of the shield:
   - LEDs
   - Buttons
   - Potmeters
   - Dallas temperature sensor
   
   
