/*--------------------------------------------------------------------
  This file is part of the HAN IoT shield library.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/

/*!
 * \file HAN_IoTShield_HWTests_v2.ino
 * \brief Sketch to test shield hardware
 * this sketch is made to test the hardware of the HAN IoT Shield
 * Code example used for OneWire DS18S20, DS18B20, and DS1822:
 * Temperature Example http://www.pjrc.com/teensy/td_libs_OneWire.html
 * \date See revision table
 *
 * \par Revision table
 *
 * Version|Date        |Note
 * -------|------------|----
 * 1      | 6-2-2019   | First released version
 * 2      | 16-3-2022  | Updated version for V2 shield
 * 
 * \par test sequence
 * 1   Led test: test leds by showing "knight rider"
 * 2.1 button test: Button press will light leds 1 and 2
 * 2.2 potmeter test: each potmeter will dim led 3 and 4 using PWM
 *     Pressing both buttons will switch to test 3
 * 8   In this test the Dallas temperature sensor will be read and presented over serial port
 * 
 */
#define RELEASE   2

#include <OneWire.h>
#include <SoftwareSerial.h>

#define PIN_LED_1_RED     4    //PD4
#define PIN_LED_2_RED     10   //PD /todo 
#define PIN_LED_3_GRN     5    //PD5
#define PIN_LED_4_GRN     6    //PD6
   
#define PIN_SWITCH_BLACK  7    // PB4
#define PIN_SWITCH_RED    3    // PB /todo 

#define RELEASED HIGH
#define PRESSED  LOW

#define PIN_SOFTSERIAL_TX 9
#define PIN_SOFTSERIAL_RX 8

SoftwareSerial softSerial(PIN_SOFTSERIAL_RX, PIN_SOFTSERIAL_TX); // RX, TX

#define PIN_POT_RED      A0
#define PIN_POT_WHITE    A1

int switchBlackState = 0;     // variable for reading the pushbutton status
int switchRedState   = 0;     // variable for reading the pushbutton status

int potRedValue   = 0;        // variable to store the value coming from the potentiometer
int potWhiteValue = 0;        // variable to store the value coming from the potentiometer

#define PIN_DALLAS      2

OneWire  ds(PIN_DALLAS);      // on pin 10 (a 4.7K resistor is necessary)

// the setup function runs once when you press reset or power the board
void setup(){
  Serial.begin(9600);
  // Wait a maximum of 10s for Serial Monitor
  while (!Serial && millis() < 10000)
    ;
    
  Serial.println("Hello, world?");
  
  softSerial.begin(9600);
  
  
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(PIN_LED_1_RED, OUTPUT);
  pinMode(PIN_LED_2_RED, OUTPUT);
  pinMode(PIN_LED_3_GRN, OUTPUT);
  pinMode(PIN_LED_4_GRN, OUTPUT);
  
  // initialize the pushbutton pin as an input:
  pinMode(PIN_SWITCH_BLACK, INPUT);
  pinMode(PIN_SWITCH_RED,   INPUT);
}

// the loop function runs over and over again forever
void loop(){

  // test 1

  Serial.println("Test 1: LEDs");
  
  for(int i = 0; i< 5; i++){
    digitalWrite(PIN_LED_1_RED, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);
    digitalWrite(PIN_LED_1_RED, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(PIN_LED_3_GRN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);
    digitalWrite(PIN_LED_3_GRN, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(PIN_LED_2_RED, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);
    digitalWrite(PIN_LED_2_RED, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(PIN_LED_4_GRN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);
    digitalWrite(PIN_LED_4_GRN, LOW);    // turn the LED off by making the voltage LOW
  }

  // test 2

  digitalWrite(PIN_LED_1_RED, HIGH);    // turn the LED off by making the voltage LOW
  digitalWrite(PIN_LED_3_GRN, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(PIN_LED_2_RED, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(PIN_LED_4_GRN, LOW);    // turn the LED off by making the voltage LOW
  Serial.println("Test 2: Potmeter and buttons");
  delay(2000);

  bool noExit = true; 
  while(noExit){
    // read the state of the pushbutton value:
    switchRedState   = digitalRead(PIN_SWITCH_RED);
    switchBlackState = digitalRead(PIN_SWITCH_BLACK);
    if ((switchRedState == PRESSED) && (switchBlackState == PRESSED)){
      noExit = false;
    }

    if(switchRedState == PRESSED){
      digitalWrite(PIN_LED_1_RED, HIGH);   // turn the LED on (HIGH is the voltage level)
    }else{ // RELEASED
      digitalWrite(PIN_LED_1_RED, LOW);   // turn the LED on (HIGH is the voltage level)
    }
  
    if(switchBlackState == PRESSED){
      digitalWrite(PIN_LED_2_RED, HIGH);   // turn the LED on (HIGH is the voltage level)
    }else{ // RELEASED
      digitalWrite(PIN_LED_2_RED, LOW);   // turn the LED on (HIGH is the voltage level)
    }

    potRedValue = analogRead(PIN_POT_RED);
    potWhiteValue = analogRead(PIN_POT_WHITE);

    analogWrite(PIN_LED_3_GRN, 1023 - potRedValue / 4);
    analogWrite(PIN_LED_4_GRN, 1023 - potWhiteValue / 4); 
  }

  digitalWrite(PIN_LED_1_RED, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(PIN_LED_3_GRN, HIGH);    // turn the LED off by making the voltage LOW
  digitalWrite(PIN_LED_2_RED, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(PIN_LED_4_GRN, LOW);    // turn the LED off by making the voltage LOW
  Serial.println("Test 3: Dallas sensor");
  delay(2000);

  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;

  while(true){
    if(!ds.search(addr)){
    Serial.println("No more addresses.");
    Serial.println();
    ds.reset_search();
    delay(250);
    //return;
    }
    
    Serial.print("ROM =");
    for( i = 0; i < 8; i++) {
      Serial.write(' ');
      Serial.print(addr[i], HEX);
    }
  
    if (OneWire::crc8(addr, 7) != addr[7]) {
        Serial.println("CRC is not valid!");
        return;
    }
    Serial.println();
   
    // the first ROM byte indicates which chip
    switch(addr[0]){
      case 0x10:
        Serial.println("  Chip = DS18S20");  // or old DS1820
        type_s = 1;
        break;
      case 0x28:
        Serial.println("  Chip = DS18B20");
        type_s = 0;
        break;
      case 0x22:
        Serial.println("  Chip = DS1822");
        type_s = 0;
        break;
      default:
        Serial.println("Device is not a DS18x20 family device.");
        return;
    } 
  
    ds.reset();
    ds.select(addr);
    ds.write(0x44, 1);        // start conversion, with parasite power on at the end
    
    delay(1000);     // maybe 750ms is enough, maybe not
    // we might do a ds.depower() here, but the reset will take care of it.
    
    present = ds.reset();
    ds.select(addr);    
    ds.write(0xBE);         // Read Scratchpad
  
    Serial.print("  Data = ");
    Serial.print(present, HEX);
    Serial.print(" ");
    for( i = 0; i < 9; i++){ // we need 9 bytes
      data[i] = ds.read();
      Serial.print(data[i], HEX);
      Serial.print(" ");
    }
    Serial.print(" CRC=");
    Serial.print(OneWire::crc8(data, 8), HEX);
    Serial.println();
  
    // Convert the data to actual temperature
    // because the result is a 16 bit signed integer, it should
    // be stored to an "int16_t" type, which is always 16 bits
    // even when compiled on a 32 bit processor.
    int16_t raw = (data[1] << 8) | data[0];
    if(type_s){
      raw = raw << 3; // 9 bit resolution default
      if(data[7] == 0x10){
        // "count remain" gives full 12 bit resolution
        raw = (raw & 0xFFF0) + 12 - data[6];
      }
    }else{
      byte cfg = (data[4] & 0x60);
      // at lower res, the low bits are undefined, so let's zero them
      if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
      else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
      else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
      //// default is 12 bit resolution, 750 ms conversion time
    }
    celsius = (float)raw / 16.0;
    fahrenheit = celsius * 1.8 + 32.0;
 
    digitalWrite(PIN_LED_1_RED, HIGH);   // turn the LED on (HIGH is the voltage level)
    
    Serial.print("  Temperature = ");
    Serial.print(celsius);
    Serial.print(" Celsius, ");
    Serial.print(fahrenheit);
    Serial.println(" Fahrenheit");

    delay(100);
    digitalWrite(PIN_LED_1_RED, LOW);    // turn the LED off by making the voltage LOW

    // read the state of the pushbutton value:
    switchRedState   = digitalRead(PIN_SWITCH_RED);
    switchBlackState = digitalRead(PIN_SWITCH_BLACK);
    if ((switchRedState == PRESSED) && (switchBlackState == PRESSED)){
      return;
    }
  }
}
