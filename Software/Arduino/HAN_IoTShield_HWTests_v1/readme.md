# Unit test for IoT shield version 1
This sketch is designed to interface with **version 1** of the shield. 

# Notes
 - The unit test will test hardware on board of the shield:
   - LEDs
   - Buttons
   - Potmeters
   - Dallas temperature sensor